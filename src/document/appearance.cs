﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xschematic.src.imp;

namespace xschematic.src.document
{
    public class Appearance
    {
        public float x { get; set; }
        private float y { get; set; }

        public float GetY()
        {
            return y;
        }

        public void SetY(float val)
        {
            y = val;
        }
    }
}
