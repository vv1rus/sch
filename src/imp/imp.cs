﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using xschematic.src.dialogs;
using xschematic.src.document;

namespace xschematic.src.imp
{
    partial class ImpBase
    {
        private MainWindow window;
        private Appearance appearance = new Appearance();
        public ImpBase(MainWindow wnd)
        {
            window = wnd;
            window.button_open.Click += handler_open_dialog;
            window.button_properties.Click += handler_properties;

            appearance.x = 5;
        }

        private void handler_open_dialog(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
            openFileDialog.Filter = "Json file (*.json) | *.json";
            if (openFileDialog.ShowDialog() == true)
            {
                appearance = JsonSerializer.Deserialize<Appearance>(File.ReadAllText(openFileDialog.FileName));
            }
        }

        private void handler_properties(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogProperties dia = new DialogProperties();
            dia.X.Text = appearance.x.ToString();
            dia.SetY(appearance.GetY());
            if (dia.ShowDialog() == true)
            {
                appearance.x = float.Parse(dia.X.Text.ToString());
                appearance.SetY(dia.GetY());
            }
        }
    }
}
