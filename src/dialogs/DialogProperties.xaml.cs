﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace xschematic.src.dialogs
{
    /// <summary>
    /// Логика взаимодействия для DialogProperties.xaml
    /// </summary>
    public partial class DialogProperties : Window
    {
        public DialogProperties()
        {
            InitializeComponent();
        }

        public void SetY(float val)
        {
            Y.Text = val.ToString();
        }

        public float GetY()
        {
            return float.Parse(Y.Text.ToString());
        }

        private void ok(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
